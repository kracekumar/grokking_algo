# -*- coding: utf-8 -*-

import collections
import math

INF = float('inf')


class Graph:
    def __init__(self):
        self.nodes = set([])
        self.edges = collections.defaultdict(list)
        self.weights = {node: INF for node in self.nodes}

    def add_node(self, name):
        self.nodes.add(name)

    def add_edge(self, from_node, to_node, val):
        assert val >= 0, "Negative value ({}) not allowed".format(val)
        self.edges[from_node].append(to_node)
        self.weights[(from_node, to_node)] = val

    def get_cost(self, start, end):
        return self.weights.get((start, end))

    def get_neighbors(self, node):
        return self.edges.get(node)

    def __str__(self):
        return "Nodes: {}, Edges: {}, Weights: {}".format(
            self.nodes, self.edges, self.weights)


def calculate_costs_and_parents(graph, start):
    assert start in graph.nodes
    costs, parents = {}, {}
    # update the neighbors with available values
    neighbors = graph.edges[start]
    for node in neighbors:
        pair = (start, node)
        costs[node] = graph.weights[pair]
        parents[node] = start
    # update non neighbors with infinity
    non_neighbors = graph.nodes - set(neighbors)
    for node in non_neighbors:
        costs[node] = INF
        parents[node] = start
    return costs, parents


def calculate_parents(graph, start):
    assert start in graph.nodes
    parents = {}
    neighbors = graph.edges[start]
    for node in neighbors:
        parents[node] = start
    # update non neighbors with infinity
    non_neighbors = graph.nodes - set(neighbors)
    for node in non_neighbors:
        parents[node] = INF
    return parents


def find_lowest_cost_node(costs, processed):
    """Find the lowest cost node which isn't processed yet.
    """
    lowest_cost = INF
    lowest_cost_node = None
    for node, cost in costs.items():
        if cost < lowest_cost and node not in processed:
            lowest_cost = cost
            lowest_cost_node = node
    return lowest_cost_node


def update_costs_and_parents(graph, costs, parents, node):
    cost = costs[node]
    neighbors = graph.get_neighbors(node)
    if not neighbors:
        return
    for neighbor in neighbors:
        new_cost = cost + graph.get_cost(start=node, end=neighbor)
        if new_cost < costs[neighbor]:
            costs[neighbor] = new_cost
            parents[neighbor] = node


def start_dijkstra(graph, start, end, costs, parents, processed, node):
    if node is None:
        return
    else:
        update_costs_and_parents(graph, costs, parents, node)
        processed.append(node)
        node = find_lowest_cost_node(costs, processed)
        start_dijkstra(graph, start, end, costs, parents, processed, node)


def dijkstra(graph, start, end):
    costs, parents = calculate_costs_and_parents(graph, start)
    processed = []
    node = find_lowest_cost_node(costs, processed)
    start_dijkstra(graph, start, end, costs, parents, processed, node)
    return costs, parents


def get_path(parents, start, end):
    node = end
    nodes = [node]
    while node is not start:
        node = parents[node]
        nodes.append(node)
    return "-> ".join(nodes[::-1])


def main(nodes, edges, start, end):
    graph = Graph()
    for node in nodes:
        graph.add_node(node)

    for edge in edges:
        graph.add_edge(edge[0], edge[1], edge[2])

    costs, parents = dijkstra(graph, start, end)
    print("Start: {}, End: {}".format(start, end))
    print("Cost: {}, Path: {}".format(costs[end], get_path(parents, start, end)))


def start():
    nodes = ['book', 'lp', 'bass_guitar', 'poster', 'drum_set', 'piano']
    edges = [('book', 'lp', 5), ('book', 'poster', 0), ('lp', 'drum_set', 20),
                 ('lp', 'bass_guitar', 15),
                 ('poster', 'bass_guitar', 30),
                 ('poster', 'drum_set', 35),
                 ('drum_set', 'piano', 10),
                 ('bass_guitar', 'piano', 20)]
    start = 'book'
    end = 'piano'
    main(nodes, edges, start, end)


def test_dijsktra():
    start()


if __name__ == "__main__":
    start()
