# -*- coding: utf-8 -*-

import operator
import math


def find_min(items):
    key = operator.itemgetter(1)
    res = min(items, key=key)
    # Most of the times weight 1 should be ok, but sometimes
    # items weigh 0.5 etc ... If minimum weight is 4, have min as 1
    # TODO: investigate why assertion fails when min is greater 1
    return min(key(res), 1)


def fill_table(table, items, total_rows, total_columns, unit):
    for row in range(1, total_rows):
        name, item_weight, value = items[row - 1]

        for col in range(1, total_columns):
            column_weight = col * unit

            if item_weight > column_weight:
                table[row][col] = table[row-1][col]
            else:
                prev_col_value = table[row-1][col]
                item_val_without_cur_item = table[row-1][col - int(item_weight/unit)]
                new_value = item_val_without_cur_item + value
                table[row][col] = max(prev_col_value, new_value)


def find_picked_items(items, table, total_rows, limit, unit):
    picked_items = []
    weight = limit

    for row in range(total_rows, 0, -1):
        position = int(weight / unit)
        is_added = table[row][position] != table[row-1][position]

        if is_added:
            picked_items.append(items[row-1])
            weight -= items[row-1][1]

    return picked_items


def knapsack(items, limit):
    unit = find_min(items)
    # First construct the grid
    total_rows = len(items) + 1
    total_columns = math.ceil(limit / unit) + 1
    table = [[0 for col in range(total_columns)] for row in range(total_rows)]

    # start filling the grid
    fill_table(table, items, total_rows, total_columns, unit)

    # Find out picked items
    return find_picked_items(items, table, total_rows - 1, limit, unit)


def gen_sum(seq, key):
    return sum(key(item) for item in seq)


def main():
    items = [('guitar', 1, 1500), ('stereo', 4, 3000), ('laptop', 3, 2000)]
    limit = 4 # in lb or kg
    result = knapsack(items, limit)
    expected_result = [items[2], items[0]]

    assert result == expected_result, "{} != {}".format(result, expected_result)
    print("items: ", items)
    print("limit: ", limit)
    print("result: ", result)
    print("picked weight: ", gen_sum(result, operator.itemgetter(2)))

    # Rosetta code: http://rosettacode.org/wiki/Knapsack_problem/0-1
    items = (
    ("map", 9, 150), ("compass", 13, 35), ("water", 153, 200), ("sandwich", 50, 160),
    ("glucose", 15, 60), ("tin", 68, 45), ("banana", 27, 60), ("apple", 39, 40),
    ("cheese", 23, 30), ("beer", 52, 10), ("suntan cream", 11, 70), ("camera", 32, 30),
    ("t-shirt", 24, 15), ("trousers", 48, 10), ("umbrella", 73, 40),
    ("waterproof trousers", 42, 70), ("waterproof overclothes", 43, 75),
    ("note-case", 22, 80), ("sunglasses", 7, 20), ("towel", 18, 12),
    ("socks", 4, 50), ("book", 30, 10),
    )
    limit = 400

    result = knapsack(items, limit)
    expected_result = [('socks', 4, 50), ('sunglasses', 7, 20), ('note-case', 22, 80), ('waterproof overclothes', 43, 75), ('waterproof trousers', 42, 70), ('suntan cream', 11, 70), ('banana', 27, 60), ('glucose', 15, 60), ('sandwich', 50, 160), ('water', 153, 200), ('compass', 13, 35), ('map', 9, 150)]

    assert result == expected_result, "{} != {}".format(result, expected_result)
    print("items: ", items)
    print("limit: ", limit)
    print("result: ", result)
    print("picked weight: ", gen_sum(result, operator.itemgetter(2)))

    # grokking algo, optimising travel itinerary
    items = [('abbey', 0.5, 7), ('theater', 0.5, 6), ('gallery', 1, 9),
            ('museum', 2, 9), ('cathedral', 0.5, 8)]
    limit = 2

    result = knapsack(items, limit)
    expected_result = [('cathedral', 0.5, 8), ('gallery', 1, 9), ('abbey', 0.5, 7)]

    assert result == expected_result, "{} != {}".format(result, expected_result)
    print("items: ", items)
    print("limit: ", limit)
    print("result: ", result)
    print("picked weight: ", gen_sum(result, operator.itemgetter(2)))


def test_knapsack():
    main()


if __name__ == "__main__":
    main()
