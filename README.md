The repo contains code snippets for standard algorithms. All Python code runs on 3.5+.
`python file.py` runs the file and asserts existing samples. You can import the file in python interpreter and pass on the value to the top level functions like `dijkstra, knapsack` etc ...

### TODO

[ ] - Document each file with entry points and parameters types.
[ ] - Support proper unit tests
